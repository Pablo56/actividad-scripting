//uScript Generated Code - Build 1.0.3104
//Generated with Debug Info
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class CapsuleScaling_U : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   delegate void ContinueExecution();
   ContinueExecution m_ContinueExecution;
   bool m_Breakpoint = false;
   const int MaxRelayCallCount = 1000;
   int relayCallCount = 0;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   UnityEngine.Vector3 local_10_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector3 local_11_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector3 local_13_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single local_2_System_Single = (float) 0;
   System.Single local_5_System_Single = (float) 0;
   UnityEngine.Vector3 local_axes_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single local_scale_unit_System_Single = (float) 1;
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_9 = null;
   UnityEngine.GameObject owner_Connection_15 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_GetDeltaTime logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_1 = new uScriptAct_GetDeltaTime( );
   System.Single logic_uScriptAct_GetDeltaTime_DeltaTime_1;
   System.Single logic_uScriptAct_GetDeltaTime_SmoothDeltaTime_1;
   System.Single logic_uScriptAct_GetDeltaTime_FixedDeltaTime_1;
   bool logic_uScriptAct_GetDeltaTime_Out_1 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyFloat_v2 logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_3 = new uScriptAct_MultiplyFloat_v2( );
   System.Single logic_uScriptAct_MultiplyFloat_v2_A_3 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_B_3 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_FloatResult_3;
   System.Int32 logic_uScriptAct_MultiplyFloat_v2_IntResult_3;
   bool logic_uScriptAct_MultiplyFloat_v2_Out_3 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyVector3WithFloat logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_6 = new uScriptAct_MultiplyVector3WithFloat( );
   UnityEngine.Vector3 logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_6 = new Vector3( );
   System.Single logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_6 = (float) 0;
   UnityEngine.Vector3 logic_uScriptAct_MultiplyVector3WithFloat_Result_6;
   bool logic_uScriptAct_MultiplyVector3WithFloat_Out_6 = true;
   //pointer to script instanced logic node
   uScriptAct_GetGameObjectScale logic_uScriptAct_GetGameObjectScale_uScriptAct_GetGameObjectScale_8 = new uScriptAct_GetGameObjectScale( );
   UnityEngine.GameObject logic_uScriptAct_GetGameObjectScale_Target_8 = default(UnityEngine.GameObject);
   UnityEngine.Vector3 logic_uScriptAct_GetGameObjectScale_Scale_8;
   System.Single logic_uScriptAct_GetGameObjectScale_X_8;
   System.Single logic_uScriptAct_GetGameObjectScale_Y_8;
   System.Single logic_uScriptAct_GetGameObjectScale_Z_8;
   bool logic_uScriptAct_GetGameObjectScale_Out_8 = true;
   //pointer to script instanced logic node
   uScriptAct_AddVector3_v2 logic_uScriptAct_AddVector3_v2_uScriptAct_AddVector3_v2_12 = new uScriptAct_AddVector3_v2( );
   UnityEngine.Vector3 logic_uScriptAct_AddVector3_v2_A_12 = new Vector3( );
   UnityEngine.Vector3 logic_uScriptAct_AddVector3_v2_B_12 = new Vector3( );
   UnityEngine.Vector3 logic_uScriptAct_AddVector3_v2_Result_12;
   bool logic_uScriptAct_AddVector3_v2_Out_12 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectScale logic_uScriptAct_SetGameObjectScale_uScriptAct_SetGameObjectScale_14 = new uScriptAct_SetGameObjectScale( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectScale_Target_14 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetGameObjectScale_Scale_14 = new Vector3( );
   bool logic_uScriptAct_SetGameObjectScale_Out_14 = true;
   //pointer to script instanced logic node
   uScriptAct_ClampVector3 logic_uScriptAct_ClampVector3_uScriptAct_ClampVector3_16 = new uScriptAct_ClampVector3( );
   UnityEngine.Vector3 logic_uScriptAct_ClampVector3_Target_16 = new Vector3( );
   System.Boolean logic_uScriptAct_ClampVector3_ClampX_16 = (bool) false;
   System.Single logic_uScriptAct_ClampVector3_XMin_16 = (float) 0;
   System.Single logic_uScriptAct_ClampVector3_XMax_16 = (float) 0;
   System.Boolean logic_uScriptAct_ClampVector3_ClampY_16 = (bool) false;
   System.Single logic_uScriptAct_ClampVector3_YMin_16 = (float) 0;
   System.Single logic_uScriptAct_ClampVector3_YMax_16 = (float) 0;
   System.Boolean logic_uScriptAct_ClampVector3_ClampZ_16 = (bool) false;
   System.Single logic_uScriptAct_ClampVector3_ZMin_16 = (float) 0;
   System.Single logic_uScriptAct_ClampVector3_ZMax_16 = (float) 0;
   UnityEngine.Vector3 logic_uScriptAct_ClampVector3_Result_16;
   bool logic_uScriptAct_ClampVector3_Out_16 = true;
   
   //event nodes
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_0 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      if ( null == owner_Connection_9 || false == m_RegisteredForEvents )
      {
         owner_Connection_9 = parentGameObject;
      }
      if ( null == owner_Connection_15 || false == m_RegisteredForEvents )
      {
         owner_Connection_15 = parentGameObject;
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
   }
   
   void SyncEventListeners( )
   {
      if ( null == event_UnityEngine_GameObject_Instance_0 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_0 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_0 )
         {
            {
               uScript_Update component = event_UnityEngine_GameObject_Instance_0.GetComponent<uScript_Update>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_0.AddComponent<uScript_Update>();
               }
               if ( null != component )
               {
                  component.OnUpdate += Instance_OnUpdate_0;
                  component.OnLateUpdate += Instance_OnLateUpdate_0;
                  component.OnFixedUpdate += Instance_OnFixedUpdate_0;
               }
            }
         }
      }
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != event_UnityEngine_GameObject_Instance_0 )
      {
         {
            uScript_Update component = event_UnityEngine_GameObject_Instance_0.GetComponent<uScript_Update>();
            if ( null != component )
            {
               component.OnUpdate -= Instance_OnUpdate_0;
               component.OnLateUpdate -= Instance_OnLateUpdate_0;
               component.OnFixedUpdate -= Instance_OnFixedUpdate_0;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_1.SetParent(g);
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_3.SetParent(g);
      logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_6.SetParent(g);
      logic_uScriptAct_GetGameObjectScale_uScriptAct_GetGameObjectScale_8.SetParent(g);
      logic_uScriptAct_AddVector3_v2_uScriptAct_AddVector3_v2_12.SetParent(g);
      logic_uScriptAct_SetGameObjectScale_uScriptAct_SetGameObjectScale_14.SetParent(g);
      logic_uScriptAct_ClampVector3_uScriptAct_ClampVector3_16.SetParent(g);
      owner_Connection_9 = parentGameObject;
      owner_Connection_15 = parentGameObject;
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      //reset each Update, and increments each method call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      if ( null != m_ContinueExecution )
      {
         ContinueExecution continueEx = m_ContinueExecution;
         m_ContinueExecution = null;
         m_Breakpoint = false;
         continueEx( );
         return;
      }
      UpdateEditorValues( );
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_OnUpdate_0(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnUpdate_0( );
   }
   
   void Instance_OnLateUpdate_0(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnLateUpdate_0( );
   }
   
   void Instance_OnFixedUpdate_0(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnFixedUpdate_0( );
   }
   
   void Relay_OnUpdate_0()
   {
      if (true == CheckDebugBreak("54a4c215-3b70-480a-8a83-24cd7b2c67eb", "Global_Update", Relay_OnUpdate_0)) return; 
      Relay_In_16();
   }
   
   void Relay_OnLateUpdate_0()
   {
      if (true == CheckDebugBreak("54a4c215-3b70-480a-8a83-24cd7b2c67eb", "Global_Update", Relay_OnLateUpdate_0)) return; 
   }
   
   void Relay_OnFixedUpdate_0()
   {
      if (true == CheckDebugBreak("54a4c215-3b70-480a-8a83-24cd7b2c67eb", "Global_Update", Relay_OnFixedUpdate_0)) return; 
   }
   
   void Relay_In_1()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("53fbc68c-9cbe-4e62-a522-144c7b331414", "Get_Delta_Time", Relay_In_1)) return; 
         {
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_1.In(out logic_uScriptAct_GetDeltaTime_DeltaTime_1, out logic_uScriptAct_GetDeltaTime_SmoothDeltaTime_1, out logic_uScriptAct_GetDeltaTime_FixedDeltaTime_1);
         local_2_System_Single = logic_uScriptAct_GetDeltaTime_DeltaTime_1;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_1.Out;
         
         if ( test_0 == true )
         {
            Relay_In_3();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleScaling_U.uscript at Get Delta Time.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_3()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("a184392c-c0d0-492c-b0bc-96f4088cddaf", "Multiply_Float", Relay_In_3)) return; 
         {
            {
               logic_uScriptAct_MultiplyFloat_v2_A_3 = local_scale_unit_System_Single;
               
            }
            {
               logic_uScriptAct_MultiplyFloat_v2_B_3 = local_2_System_Single;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_3.In(logic_uScriptAct_MultiplyFloat_v2_A_3, logic_uScriptAct_MultiplyFloat_v2_B_3, out logic_uScriptAct_MultiplyFloat_v2_FloatResult_3, out logic_uScriptAct_MultiplyFloat_v2_IntResult_3);
         local_5_System_Single = logic_uScriptAct_MultiplyFloat_v2_FloatResult_3;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_3.Out;
         
         if ( test_0 == true )
         {
            Relay_In_6();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleScaling_U.uscript at Multiply Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_6()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("9174b3eb-c5fe-4f68-ad7f-20ee11fc5a81", "Multiply_Vector3_With_Float", Relay_In_6)) return; 
         {
            {
               logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_6 = local_axes_UnityEngine_Vector3;
               
            }
            {
               logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_6 = local_5_System_Single;
               
            }
            {
            }
         }
         logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_6.In(logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_6, logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_6, out logic_uScriptAct_MultiplyVector3WithFloat_Result_6);
         local_10_UnityEngine_Vector3 = logic_uScriptAct_MultiplyVector3WithFloat_Result_6;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_6.Out;
         
         if ( test_0 == true )
         {
            Relay_In_8();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleScaling_U.uscript at Multiply Vector3 With Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_8()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("4d09fb30-a6bd-4d2e-a092-2ea3bc913b41", "Get_Scale", Relay_In_8)) return; 
         {
            {
               logic_uScriptAct_GetGameObjectScale_Target_8 = owner_Connection_9;
               
            }
            {
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_GetGameObjectScale_uScriptAct_GetGameObjectScale_8.In(logic_uScriptAct_GetGameObjectScale_Target_8, out logic_uScriptAct_GetGameObjectScale_Scale_8, out logic_uScriptAct_GetGameObjectScale_X_8, out logic_uScriptAct_GetGameObjectScale_Y_8, out logic_uScriptAct_GetGameObjectScale_Z_8);
         local_11_UnityEngine_Vector3 = logic_uScriptAct_GetGameObjectScale_Scale_8;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_GetGameObjectScale_uScriptAct_GetGameObjectScale_8.Out;
         
         if ( test_0 == true )
         {
            Relay_In_12();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleScaling_U.uscript at Get Scale.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_12()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("4d76b07e-c6e5-4f57-b245-6ae73cccf9ea", "Add_Vector3", Relay_In_12)) return; 
         {
            {
               logic_uScriptAct_AddVector3_v2_A_12 = local_10_UnityEngine_Vector3;
               
            }
            {
               logic_uScriptAct_AddVector3_v2_B_12 = local_11_UnityEngine_Vector3;
               
            }
            {
            }
         }
         logic_uScriptAct_AddVector3_v2_uScriptAct_AddVector3_v2_12.In(logic_uScriptAct_AddVector3_v2_A_12, logic_uScriptAct_AddVector3_v2_B_12, out logic_uScriptAct_AddVector3_v2_Result_12);
         local_13_UnityEngine_Vector3 = logic_uScriptAct_AddVector3_v2_Result_12;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleScaling_U.uscript at Add Vector3.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_14()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("d2e5f049-f28a-40c7-bcd8-52ebec956a49", "Set_Scale", Relay_In_14)) return; 
         {
            {
               int index = 0;
               if ( logic_uScriptAct_SetGameObjectScale_Target_14.Length <= index)
               {
                  System.Array.Resize(ref logic_uScriptAct_SetGameObjectScale_Target_14, index + 1);
               }
               logic_uScriptAct_SetGameObjectScale_Target_14[ index++ ] = owner_Connection_15;
               
            }
            {
               logic_uScriptAct_SetGameObjectScale_Scale_14 = local_13_UnityEngine_Vector3;
               
            }
         }
         logic_uScriptAct_SetGameObjectScale_uScriptAct_SetGameObjectScale_14.In(logic_uScriptAct_SetGameObjectScale_Target_14, logic_uScriptAct_SetGameObjectScale_Scale_14);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleScaling_U.uscript at Set Scale.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_16()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("f7a34eb5-9c48-4eb0-8e6b-4f34cc2c8f2d", "Clamp_Vector3", Relay_In_16)) return; 
         {
            {
               logic_uScriptAct_ClampVector3_Target_16 = local_axes_UnityEngine_Vector3;
               
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_ClampVector3_uScriptAct_ClampVector3_16.In(logic_uScriptAct_ClampVector3_Target_16, logic_uScriptAct_ClampVector3_ClampX_16, logic_uScriptAct_ClampVector3_XMin_16, logic_uScriptAct_ClampVector3_XMax_16, logic_uScriptAct_ClampVector3_ClampY_16, logic_uScriptAct_ClampVector3_YMin_16, logic_uScriptAct_ClampVector3_YMax_16, logic_uScriptAct_ClampVector3_ClampZ_16, logic_uScriptAct_ClampVector3_ZMin_16, logic_uScriptAct_ClampVector3_ZMax_16, out logic_uScriptAct_ClampVector3_Result_16);
         local_axes_UnityEngine_Vector3 = logic_uScriptAct_ClampVector3_Result_16;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_ClampVector3_uScriptAct_ClampVector3_16.Out;
         
         if ( test_0 == true )
         {
            Relay_In_1();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleScaling_U.uscript at Clamp Vector3.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   private void UpdateEditorValues( )
   {
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleScaling_U.uscript:2", local_2_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "623cd987-bc06-444b-b0bd-12084beda74d", local_2_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleScaling_U.uscript:scale unit", local_scale_unit_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "01c5abe4-2dd1-4c15-bfee-9d40d859dd0f", local_scale_unit_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleScaling_U.uscript:5", local_5_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "622e2f88-3d93-4d75-9f91-97e66ba60cb0", local_5_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleScaling_U.uscript:axes", local_axes_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "4dad1ff1-409b-4d00-8b09-ce47c18f03cd", local_axes_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleScaling_U.uscript:10", local_10_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "10ccbb0e-1731-4d1f-bf6b-b5df2439b9c0", local_10_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleScaling_U.uscript:11", local_11_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "80179549-3b83-4f35-8394-e3829e149673", local_11_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleScaling_U.uscript:13", local_13_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "e86e7807-4d64-48a5-aa94-6fe2d2e48a4c", local_13_UnityEngine_Vector3);
   }
   bool CheckDebugBreak(string guid, string name, ContinueExecution method)
   {
      if (true == m_Breakpoint) return true;
      
      if (true == uScript_MasterComponent.FindBreakpoint(guid))
      {
         if (uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint == guid)
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = "";
         }
         else
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = guid;
            UpdateEditorValues( );
            UnityEngine.Debug.Log("uScript BREAK Node:" + name + " ((Time: " + Time.time + "");
            UnityEngine.Debug.Break();
            #if (!UNITY_FLASH)
            m_ContinueExecution = new ContinueExecution(method);
            #endif
            m_Breakpoint = true;
            return true;
         }
      }
      return false;
   }
}
