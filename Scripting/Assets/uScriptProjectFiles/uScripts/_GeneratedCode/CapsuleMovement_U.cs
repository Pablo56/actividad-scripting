//uScript Generated Code - Build 1.0.3104
//Generated with Debug Info
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class CapsuleMovement_U : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   delegate void ContinueExecution();
   ContinueExecution m_ContinueExecution;
   bool m_Breakpoint = false;
   const int MaxRelayCallCount = 1000;
   int relayCallCount = 0;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   public UnityEngine.Vector3 direction = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector3 local_11_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single local_6_System_Single = (float) 0;
   System.Single local_8_System_Single = (float) 0;
   public System.Single speed = (float) 1;
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_10 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_ClampVector3 logic_uScriptAct_ClampVector3_uScriptAct_ClampVector3_3 = new uScriptAct_ClampVector3( );
   UnityEngine.Vector3 logic_uScriptAct_ClampVector3_Target_3 = new Vector3( );
   System.Boolean logic_uScriptAct_ClampVector3_ClampX_3 = (bool) true;
   System.Single logic_uScriptAct_ClampVector3_XMin_3 = (float) -1;
   System.Single logic_uScriptAct_ClampVector3_XMax_3 = (float) 1;
   System.Boolean logic_uScriptAct_ClampVector3_ClampY_3 = (bool) true;
   System.Single logic_uScriptAct_ClampVector3_YMin_3 = (float) -1;
   System.Single logic_uScriptAct_ClampVector3_YMax_3 = (float) 1;
   System.Boolean logic_uScriptAct_ClampVector3_ClampZ_3 = (bool) true;
   System.Single logic_uScriptAct_ClampVector3_ZMin_3 = (float) -1;
   System.Single logic_uScriptAct_ClampVector3_ZMax_3 = (float) 1;
   UnityEngine.Vector3 logic_uScriptAct_ClampVector3_Result_3;
   bool logic_uScriptAct_ClampVector3_Out_3 = true;
   //pointer to script instanced logic node
   uScriptAct_GetDeltaTime logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_4 = new uScriptAct_GetDeltaTime( );
   System.Single logic_uScriptAct_GetDeltaTime_DeltaTime_4;
   System.Single logic_uScriptAct_GetDeltaTime_SmoothDeltaTime_4;
   System.Single logic_uScriptAct_GetDeltaTime_FixedDeltaTime_4;
   bool logic_uScriptAct_GetDeltaTime_Out_4 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyFloat_v2 logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_5 = new uScriptAct_MultiplyFloat_v2( );
   System.Single logic_uScriptAct_MultiplyFloat_v2_A_5 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_B_5 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_FloatResult_5;
   System.Int32 logic_uScriptAct_MultiplyFloat_v2_IntResult_5;
   bool logic_uScriptAct_MultiplyFloat_v2_Out_5 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyVector3WithFloat logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_7 = new uScriptAct_MultiplyVector3WithFloat( );
   UnityEngine.Vector3 logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_7 = new Vector3( );
   System.Single logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_7 = (float) 0;
   UnityEngine.Vector3 logic_uScriptAct_MultiplyVector3WithFloat_Result_7;
   bool logic_uScriptAct_MultiplyVector3WithFloat_Out_7 = true;
   
   //event nodes
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_0 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   UnityEngine.Vector3 method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_translation_9 = new Vector3( );
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      if ( null == owner_Connection_10 || false == m_RegisteredForEvents )
      {
         owner_Connection_10 = parentGameObject;
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
   }
   
   void SyncEventListeners( )
   {
      if ( null == event_UnityEngine_GameObject_Instance_0 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_0 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_0 )
         {
            {
               uScript_Update component = event_UnityEngine_GameObject_Instance_0.GetComponent<uScript_Update>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_0.AddComponent<uScript_Update>();
               }
               if ( null != component )
               {
                  component.OnUpdate += Instance_OnUpdate_0;
                  component.OnLateUpdate += Instance_OnLateUpdate_0;
                  component.OnFixedUpdate += Instance_OnFixedUpdate_0;
               }
            }
         }
      }
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != event_UnityEngine_GameObject_Instance_0 )
      {
         {
            uScript_Update component = event_UnityEngine_GameObject_Instance_0.GetComponent<uScript_Update>();
            if ( null != component )
            {
               component.OnUpdate -= Instance_OnUpdate_0;
               component.OnLateUpdate -= Instance_OnLateUpdate_0;
               component.OnFixedUpdate -= Instance_OnFixedUpdate_0;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_ClampVector3_uScriptAct_ClampVector3_3.SetParent(g);
      logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_4.SetParent(g);
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_5.SetParent(g);
      logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_7.SetParent(g);
      owner_Connection_10 = parentGameObject;
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      //reset each Update, and increments each method call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      if ( null != m_ContinueExecution )
      {
         ContinueExecution continueEx = m_ContinueExecution;
         m_ContinueExecution = null;
         m_Breakpoint = false;
         continueEx( );
         return;
      }
      UpdateEditorValues( );
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_OnUpdate_0(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnUpdate_0( );
   }
   
   void Instance_OnLateUpdate_0(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnLateUpdate_0( );
   }
   
   void Instance_OnFixedUpdate_0(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnFixedUpdate_0( );
   }
   
   void Relay_OnUpdate_0()
   {
      if (true == CheckDebugBreak("c0bc9bba-f19a-4d02-a368-eb5938f2e7fd", "Global_Update", Relay_OnUpdate_0)) return; 
      Relay_In_3();
   }
   
   void Relay_OnLateUpdate_0()
   {
      if (true == CheckDebugBreak("c0bc9bba-f19a-4d02-a368-eb5938f2e7fd", "Global_Update", Relay_OnLateUpdate_0)) return; 
   }
   
   void Relay_OnFixedUpdate_0()
   {
      if (true == CheckDebugBreak("c0bc9bba-f19a-4d02-a368-eb5938f2e7fd", "Global_Update", Relay_OnFixedUpdate_0)) return; 
   }
   
   void Relay_In_3()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("bdf2a3f0-6072-457e-8b8b-8b55a5c8a7f0", "Clamp_Vector3", Relay_In_3)) return; 
         {
            {
               logic_uScriptAct_ClampVector3_Target_3 = direction;
               
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_ClampVector3_uScriptAct_ClampVector3_3.In(logic_uScriptAct_ClampVector3_Target_3, logic_uScriptAct_ClampVector3_ClampX_3, logic_uScriptAct_ClampVector3_XMin_3, logic_uScriptAct_ClampVector3_XMax_3, logic_uScriptAct_ClampVector3_ClampY_3, logic_uScriptAct_ClampVector3_YMin_3, logic_uScriptAct_ClampVector3_YMax_3, logic_uScriptAct_ClampVector3_ClampZ_3, logic_uScriptAct_ClampVector3_ZMin_3, logic_uScriptAct_ClampVector3_ZMax_3, out logic_uScriptAct_ClampVector3_Result_3);
         direction = logic_uScriptAct_ClampVector3_Result_3;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_ClampVector3_uScriptAct_ClampVector3_3.Out;
         
         if ( test_0 == true )
         {
            Relay_In_4();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleMovement_U.uscript at Clamp Vector3.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_4()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("55227463-a7a7-430a-ad41-e3cec0d47ab8", "Get_Delta_Time", Relay_In_4)) return; 
         {
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_4.In(out logic_uScriptAct_GetDeltaTime_DeltaTime_4, out logic_uScriptAct_GetDeltaTime_SmoothDeltaTime_4, out logic_uScriptAct_GetDeltaTime_FixedDeltaTime_4);
         local_6_System_Single = logic_uScriptAct_GetDeltaTime_DeltaTime_4;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_4.Out;
         
         if ( test_0 == true )
         {
            Relay_In_5();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleMovement_U.uscript at Get Delta Time.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_5()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("5beac38d-4e75-43df-a8af-f6d48eff92bf", "Multiply_Float", Relay_In_5)) return; 
         {
            {
               logic_uScriptAct_MultiplyFloat_v2_A_5 = speed;
               
            }
            {
               logic_uScriptAct_MultiplyFloat_v2_B_5 = local_6_System_Single;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_5.In(logic_uScriptAct_MultiplyFloat_v2_A_5, logic_uScriptAct_MultiplyFloat_v2_B_5, out logic_uScriptAct_MultiplyFloat_v2_FloatResult_5, out logic_uScriptAct_MultiplyFloat_v2_IntResult_5);
         local_8_System_Single = logic_uScriptAct_MultiplyFloat_v2_FloatResult_5;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_5.Out;
         
         if ( test_0 == true )
         {
            Relay_In_7();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleMovement_U.uscript at Multiply Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_7()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("129f2950-6ef5-47ea-a787-a0efb46e63cc", "Multiply_Vector3_With_Float", Relay_In_7)) return; 
         {
            {
               logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_7 = direction;
               
            }
            {
               logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_7 = local_8_System_Single;
               
            }
            {
            }
         }
         logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_7.In(logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_7, logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_7, out logic_uScriptAct_MultiplyVector3WithFloat_Result_7);
         local_11_UnityEngine_Vector3 = logic_uScriptAct_MultiplyVector3WithFloat_Result_7;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_7.Out;
         
         if ( test_0 == true )
         {
            Relay_Translate_9();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleMovement_U.uscript at Multiply Vector3 With Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Translate_9()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("bbcce970-586c-4a35-818c-f2a020e529f6", "UnityEngine_Transform", Relay_Translate_9)) return; 
         {
            {
               method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_translation_9 = local_11_UnityEngine_Vector3;
               
            }
         }
         {
            UnityEngine.Transform component;
            component = owner_Connection_10.GetComponent<UnityEngine.Transform>();
            if ( null != component )
            {
               component.Translate(method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_translation_9);
            }
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleMovement_U.uscript at UnityEngine.Transform.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   private void UpdateEditorValues( )
   {
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleMovement_U.uscript:speed", speed);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "0e8e845d-bfae-4bd3-835c-e691ebe7ef8d", speed);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleMovement_U.uscript:direction", direction);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "6ce8af1a-a482-466d-ae41-f0433d9327d0", direction);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleMovement_U.uscript:6", local_6_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "9890e75a-5502-4908-998b-aa193604319f", local_6_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleMovement_U.uscript:8", local_8_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "a3385364-c150-4578-b4fa-8692d0d0df2f", local_8_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleMovement_U.uscript:11", local_11_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "b90da61d-6361-4d89-9d9a-5704d743f4c3", local_11_UnityEngine_Vector3);
   }
   bool CheckDebugBreak(string guid, string name, ContinueExecution method)
   {
      if (true == m_Breakpoint) return true;
      
      if (true == uScript_MasterComponent.FindBreakpoint(guid))
      {
         if (uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint == guid)
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = "";
         }
         else
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = guid;
            UpdateEditorValues( );
            UnityEngine.Debug.Log("uScript BREAK Node:" + name + " ((Time: " + Time.time + "");
            UnityEngine.Debug.Break();
            #if (!UNITY_FLASH)
            m_ContinueExecution = new ContinueExecution(method);
            #endif
            m_Breakpoint = true;
            return true;
         }
      }
      return false;
   }
}
